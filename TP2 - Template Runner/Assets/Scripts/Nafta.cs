using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nafta : MonoBehaviour
{
    public ParticleSystem particulas;
    public float tiempoAumentado = 5f;
    public static float naftaVelocity;
    private Rigidbody rb;

    
    private void Start()
    {
        rb = GetComponent<Rigidbody>();

        
    }
    private void Update()
    {
        //nafta se mueve con el mapa
        rb.AddForce(new Vector3(-naftaVelocity, 0, 0), ForceMode.Force);
        OutOfBounds();


        if (Controller_Hud.gameOver)
        {
            Destroy(this.gameObject);
        }
    }



    private void OnTriggerEnter(Collider other)
    {

        GameObject jugador = GameObject.Find("Player");

        if (other.gameObject == jugador)
        {
            ControlBarrita barraTiempo = GameObject.Find("barrita").GetComponent<ControlBarrita>();
            barraTiempo.AumentarTiempo(tiempoAumentado);
            Instantiate(particulas, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
       
    }


   

    

    //si se sale se destruye
    public void OutOfBounds()
    {
        if (this.transform.position.x <= -15)
        {
            Destroy(this.gameObject);
        }
    }
}
