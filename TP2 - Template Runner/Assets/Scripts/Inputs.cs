using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inputs : MonoBehaviour
{
    private SkinColor skinColor;

    private Controller_Player control_player;

    private Restart restart;
    void Start()
    {
        skinColor = GetComponent<SkinColor>();
        control_player = GetComponent<Controller_Player>();
        restart = GameObject.Find("GameManager").GetComponent<Restart>();
    }

    // Update is called once per frame
    void Update()
    {
        control_player.PlayerInputs();

        restart.RestartInput();

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            skinColor.setColor(0);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            skinColor.setColor(1);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            skinColor.setColor(2);
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            skinColor.setColor(3);
        }
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            skinColor.setColor(4);
        }





    }
}
