using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class SaveManager : MonoBehaviour
{
    public static SaveManager instancia; 
    public RecordData data; 
    string archivoDatos = "save.dat";

    private void Awake() 
    {
        if (instancia == null) 
        {
            DontDestroyOnLoad(this.gameObject); 
            instancia = this; 
        } 
        else if (instancia != this) 
            Destroy(this.gameObject); 
        
        LoadGameData(); 
    }

    public void SavePlayerData()
    {
        string filePath = Application.persistentDataPath + "/" + archivoDatos; 
        BinaryFormatter bf = new BinaryFormatter(); 
        FileStream file = File.Create(filePath); 
        bf.Serialize(file, data); 
        file.Close(); 
        Debug.Log("Datos guardados");
    }

    public void LoadGameData()
    {
        string filePath = Application.persistentDataPath + "/" + archivoDatos; 
        BinaryFormatter bf = new BinaryFormatter(); 
        
        if (File.Exists(filePath)) 
        { 
            FileStream file = File.Open(filePath, FileMode.Open); 
            RecordData cargado = (RecordData)bf.Deserialize(file); 
            data = cargado; 
            file.Close(); 
            Debug.Log("Datos cargados"); 
        }
    }
}

[System.Serializable]
public class RecordData
{
    public float record;

    public RecordData()
    {
        record = 0;
    }
}
