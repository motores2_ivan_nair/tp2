﻿using UnityEngine;

public class Controller_Player : MonoBehaviour
{
    private Rigidbody rb;
    public float jumpForce = 10;
    private float initialSize;
    private int i = 0;
    private bool floored;

    //private float time;


    private rayoLaser rayolaser;
    private float laserTimer;
    public int CDLaser=3;
   
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        initialSize = rb.transform.localScale.y;

        laserTimer = CDLaser;


        //time = 10;
    }

    private void Update()
    {

        if (laserTimer <= CDLaser)
        {
            laserTimer += Time.deltaTime;
        }


        //Timer();
        PlayerInputs();

        rayolaser = GameObject.Find("rayoLaser").GetComponent<rayoLaser>();
        
    }

   public void PlayerInputs()
    {
        Jump();
        Duck();
        DispararRayo();
    }

    private void Jump()
    {
        if (floored)
        {
            if (Input.GetKeyDown(KeyCode.W))
            {
                rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
                //rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
            }
        }
    }

    private void Duck()
    {
        if (floored)
        {
            if (Input.GetKey(KeyCode.S))
            {
                if (i == 0)
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, rb.transform.localScale.y / 2, rb.transform.localScale.z);
                    rb.AddForce(Vector3.down * 3f, ForceMode.Impulse);
                    i++;
                }
            }
            else
            {
                if (rb.transform.localScale.y != initialSize)
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, initialSize, rb.transform.localScale.z);
                    i = 0;
                }
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.S))
            {
                rb.AddForce(new Vector3(0, -jumpForce, 0), ForceMode.Impulse);
            }
        }
    }

    private void DispararRayo()
    {
        if (Input.GetKeyDown(KeyCode.Q) && laserTimer>= CDLaser)
        {
            rayolaser.instanciarRayo();
            
            laserTimer = 0;
        }

    }


    //private void Timer()
    //{
      
    //    time -= Time.deltaTime;

    //    Debug.Log("Timer: " + time);

    //    //perder cuando timer <=0
    //    if (time <= 0)
    //    {
    //        time = 0;
    //        Controller_Hud.gameOver = true;
    //    }
  
    //}

    //private void SumarAlTimer(int sumar)
    //{
    //    time += sumar;


    //   // para que nunca se pase de 10
    //    if (time > 10)
    //    {
    //        time = 10;
    //    }

    //}

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            Destroy(this.gameObject);
            Controller_Hud.gameOver = true;
        }

        if (collision.gameObject.CompareTag("Agua"))
        {
            Destroy(this.gameObject);
            Controller_Hud.gameOver = true;
        }

        if (collision.gameObject.CompareTag("Floor"))
        {
            floored = true;
        }

        if (collision.gameObject.CompareTag("Nafta"))
        {
           //SumarAlTimer(5);
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            floored = false;
        }
    }
}
