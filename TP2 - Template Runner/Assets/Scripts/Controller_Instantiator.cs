﻿using System.Collections.Generic;
using UnityEngine;

public class Controller_Instantiator : MonoBehaviour
{
    public List<GameObject> enemies;
    public List<GameObject> nafta;
    public GameObject agua;

    [Header("Otros datos")]
    public GameObject instantiatePos;
    public GameObject instantiatePos2;
    public float respawningTimer;
    public float respawningTimer2;
    public float respawningTimer3;
    private float time = 0;
    private bool activar_nafta = false;


    void Start()
    {
        Controller_Enemy.enemyVelocity = 2;
        Nafta.naftaVelocity = 2;
        Agua.aguaVelocity = 2;
        Moneda.monedaSpeed = 2;
    }

    void Update()
    {
        SpawnEnemies();
        SpawnNaftas();
        SpawnAgua();
        ChangeVelocity();
    }

    private void ChangeVelocity()
    {
        time += Time.deltaTime;
       
        Controller_Enemy.enemyVelocity = Mathf.SmoothStep(1f, 13f, time / 180f);
        Nafta.naftaVelocity = Mathf.SmoothStep(1f, 13f, time / 180f);
        Agua.aguaVelocity= Mathf.SmoothStep(1f, 13f, time / 180f);
        Moneda.monedaSpeed = Mathf.SmoothStep(1f, 13f, time / 180f);
    }

    private void SpawnEnemies()
    {
        respawningTimer -= Time.deltaTime;


       

        if (respawningTimer <= 0 )
        {
            Instantiate(enemies[UnityEngine.Random.Range(0, enemies.Count)], instantiatePos.transform);
            respawningTimer = UnityEngine.Random.Range(4, 8);
            
        }
           
    }

    private void SpawnNaftas()
    {

        respawningTimer2 -= Time.deltaTime;

        if (respawningTimer2 <= 0 )
        {

            Instantiate(nafta[UnityEngine.Random.Range(0, nafta.Count)], instantiatePos2.transform);
            respawningTimer2 = UnityEngine.Random.Range(0.5f, 4);

           

        }

    }

    private void SpawnAgua()
    {
        respawningTimer3 -= Time.deltaTime;

        if (respawningTimer3 <= 0)
        {

            Instantiate(agua, instantiatePos2.transform);
            respawningTimer3 = UnityEngine.Random.Range(10, 15);



        }
    }

    //private void SpawnMonedas()
    //{
    //    respawningTimer4 -= Time.deltaTime;

    //    if (respawningTimer4 <= 0)
    //    {
    //        Instantiate(moneda, instantiatePos2.transform);
    //        respawningTimer4 = UnityEngine.Random.Range(0.5f, 4);

    //    }
    //}
}
