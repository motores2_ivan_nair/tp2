using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlBarrita : MonoBehaviour
{
    public float tiempoInicial = 10f;
    public RectTransform barraTransform;

    private float tiempoRestante;
    void Start()
    {
        tiempoRestante = tiempoInicial;
    }

    // Update is called once per frame
    void Update()
    {
        //tiempo baja
        tiempoRestante -= Time.deltaTime;
        
        //barra decrece en escala con interpolacion
        float escala = Mathf.Lerp(0f, 1f, tiempoRestante / tiempoInicial);
        barraTransform.localScale = new Vector3(escala, 1f, 1f);


        if (tiempoRestante <= 0)
        {
           
            Controller_Hud.gameOver = true;
        }
    }


    public void AumentarTiempo(float cantidad)
    {
        tiempoRestante += cantidad;
        tiempoRestante = Mathf.Clamp(tiempoRestante, 0f, tiempoInicial);
    }
}

