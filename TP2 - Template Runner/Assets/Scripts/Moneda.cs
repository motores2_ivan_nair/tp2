using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Moneda : MonoBehaviour
{
    public static float monedaSpeed;
    private Rigidbody rb;
    private int monedas;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        rb.AddForce(new Vector3(-monedaSpeed, 0, 0), ForceMode.Force);
        OutOfBounds();


        if (Controller_Hud.gameOver)
        {
            Destroy(this.gameObject);
        }
    }

    public void OutOfBounds()
    {
        if (this.transform.position.x <= -15)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {

        GameObject jugador = GameObject.Find("Player");

        if (other.gameObject == jugador)
        {
            monedas += 1;
            //Instantiate(particulas, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }

    }
}
