using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkinColor : MonoBehaviour
{
    public Color[] colores;
    public GameObject objectoACambiar;

    // Update is called once per frame
    void Update()
    {
    }

    public void setColor(int index)
    {
        Renderer objectRender = objectoACambiar.GetComponent<Renderer>(); //renderer del objeto que queremos

        Material material = objectRender.material; //material del renderer

        material.color = colores[index]; //Asigna nuevo color

        objectRender.material = material; //asigna el nuevo material
    }
}
