using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rayoLaser : MonoBehaviour
{

    private GameObject ObjLaser;
    private bool rayoInstanciado;

    void Start()
    {
        ObjLaser = GameObject.Find("Laser");
        ObjLaser.SetActive(false);
       
        rayoInstanciado=false;
        gameObject.GetComponent<BoxCollider>().isTrigger = true;
    }


    private void Update()
    {
        //un vector que agarra la pos del rayo
        Vector3 nuevaPosicion=transform.position;

        //pero en Y agarra la pos.y del jugador
        try
        {
            nuevaPosicion.y = GameObject.Find("Player").transform.position.y;
        }
        catch
        {
            nuevaPosicion = transform.position;


        }
        
        transform.position =nuevaPosicion;
        
        
        //el trigger que elimina enemigos se activa solo al estar el rayo activo
        if (rayoInstanciado)
        {
            gameObject.GetComponent<BoxCollider>().isTrigger = false;
        }
        else
        {
            gameObject.GetComponent<BoxCollider>().isTrigger = true;
        }


        //se desactiva si se pierde
        if (Controller_Hud.gameOver)
        {
            ObjLaser.SetActive(false);
        }
    }

    public void instanciarRayo()
    {
       
        ObjLaser.SetActive(true);
        rayoInstanciado = true;


        Invoke("DesactivarRayo", 1f);
    }

    void DesactivarRayo()
    {
      
        ObjLaser.SetActive(false);
        rayoInstanciado = false;
    }

    

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Enemy" && rayoInstanciado)
        {
            Destroy(collision.gameObject);
        }
    }


}
