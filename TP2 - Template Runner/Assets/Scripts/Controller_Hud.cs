﻿using UnityEngine;
using UnityEngine.UI;

public class Controller_Hud : MonoBehaviour
{
    public static bool gameOver = false;
    public Text distanceText;
    public Text gameOverText;
    public Text newRecordText;
    public float distance = 0;
    public float highScore = 0;
    public SaveManager saveManager;
  
    private GameObject barrita;

    void Start()
    {
        saveManager = FindObjectOfType<SaveManager>();
        highScore = saveManager.data.record;
        gameOver = false;

        distance = 0;
        distanceText.text = distance.ToString();
        gameOverText.gameObject.SetActive(false);
        newRecordText.gameObject.SetActive(false);

    }

    void Update()
    {
        if (gameOver)
        {

            if(distance > highScore)
            {
                highScore = distance;
                saveManager.data.record = highScore;
                newRecordText.text = "Record: " + highScore.ToString("F0");
                newRecordText.gameObject.SetActive(true);
            }

            saveManager.SavePlayerData();
            Time.timeScale = 0;
            gameOverText.text = "Game Over \n Total Distance: " + distance.ToString("F0");
            gameOverText.gameObject.SetActive(true);
            newRecordText.text = "Record: " + highScore.ToString("F0");
            newRecordText.gameObject.SetActive(true);

        }
        else
        {
            distance +=Time.deltaTime*6;
            distanceText.text = distance.ToString("F0");
        }

    }
}
